
# --- 1 -----

import numpy as np
import time
import os

from openmdao.core.mpi_wrap import MPI
from openmdao.api import Problem, Group, IndepVarComp, ParallelFDGroup, Component
from openmdao.api import ScipyOptimizer, ExecComp
from openmdao.solvers.ln_gauss_seidel import LinearGaussSeidel

if MPI:
    from openmdao.core.petsc_impl import PetscImpl as impl
else:
    from openmdao.core.basic_impl import BasicImpl as impl

from openmdao.drivers.pyoptsparse_driver import pyOptSparseDriver


from fusedwind.turbine.geometry import read_blade_planform,\
                                       redistribute_planform,\
                                       PGLLoftedBladeSurface,\
                                       SplinedBladePlanform, \
                                       PGLRedistributedPlanform
from fusedwind.turbine.structure import read_bladestructure, \
                                        write_bladestructure, \
                                        interpolate_bladestructure, \
                                        SplinedBladeStructure, \
                                        BladeStructureProperties
from fusedwind.turbine.recorders import get_planform_recording_vars, \
                                        get_structure_recording_vars

# from hawtopt2.main import HawtOpt2Main
from becas_wrapper.becas_bladestructure import BECASBeamStructure
from becas_wrapper.becas_stressrecovery import BECASStressRecovery
from hawc2_wrapper.hawc2s_aeroelasticsolver import HAWC2SAeroElasticSolver
from hawc2_wrapper.hawc2_aeroelasticsolver import HAWC2AeroElasticSolver

from sqlitedict import SqliteDict

user_home = os.getenv('HOME')


class WeibullAEP(Component):

    def __init__(self, size, wsp_array, A=8., k=2.):
        super(WeibullAEP, self).__init__()

        self.wsp_array = wsp_array
        self.A = A
        self.k = k
        self.exclude = []
        self.wspIn = wsp_array[0]
        self.wspOut = wsp_array[-1]

        self.add_param('power_curve', np.zeros(size))
        self.add_output('aep', 0.)

    def solve_nonlinear(self, params, unknowns, resids):

        cdf_ws = lambda A,k,u, bin=1.0: np.exp(-(max(0.0, u-bin/2.0)/A)**k) - np.exp(-((u+bin/2.0)/A)**k)

        ws_f = []
        ps = []
        wsps = []
        for i, ws in enumerate(self.wsp_array):
            if i in self.exclude: continue
            if ws > self.wspOut: continue
            if ws < self.wspIn: continue
            ws_f.append(cdf_ws(self.A, self.k, ws))
            ps.append(params['power_curve'][i])
            wsps.append(ws)
        self.ws_f = np.asarray(ws_f)
        ps = np.asarray(ps)
        wsps = np.asarray(wsps)

        unknowns['aep'] = np.trapz(ps * self.ws_f * 8760.0, wsps)
        
class H2PowerCurve(Component):

    def __init__(self, nwsp, nchannels, ncases, ch_nr):
        super(H2PowerCurve, self).__init__()

        self.nwsp = nwsp
        self.ch_nr = ch_nr

        self.add_param('mean_stats', shape=[ncases,nchannels])
        self.add_output('P', np.zeros(nwsp))

    def solve_nonlinear(self, params, unknowns, resids):

        unknowns['P'] = params['mean_stats'][:self.nwsp,self.ch_nr]
        
class CorrectedEnvelopes(Component):

    def __init__(self, nsec, nsectors, full_dict, red_dict):
        super(CorrectedEnvelopes, self).__init__()

        self.full_dict = full_dict
        self.red_dict = red_dict
        self.nsec = nsec
        
        self.envelope_corr_var = []
        
        db_full = SqliteDict(self.full_dict, 'openmdao' )
        self.us_full = db_full['rank0:Driver/1']['Unknowns']

        db_red = SqliteDict(self.red_dict, 'openmdao' )
        self.us_red = db_red['rank0:Driver/1']['Unknowns']
        
        for isec in range(nsec):
            p_name = 'blade_loads_envelope_sec%03d_ori' %isec
            self.add_param(p_name, shape=[nsectors, 6])
            name = 'blade_loads_envelope_sec%03d_corr' %isec
            self.envelope_corr_var.append(name)
            self.add_output(name, shape=[nsectors, 6])

    def solve_nonlinear(self, params, unknowns, resids):
              
        for i, var in enumerate(self.envelope_corr_var):
            theta = params['blade_loads_envelope_sec%03d_ori'%i]\
                           /self.us_red['blade_loads_envelope_sec%03d'%i]\
                           *self.us_full['blade_loads_envelope_sec%03d'%i]
            unknowns[var] = np.nan_to_num(theta)


optimize_flag = True
dry_run = False
FPM = True
par_fd = 25

if optimize_flag:
    top = Problem(impl=impl, root=ParallelFDGroup(par_fd))
else:
    top = Problem(impl=impl, root=Group())

root = top.root
root.ln_solver = LinearGaussSeidel()
root.ln_solver.options['single_voi_relevance_reduction'] = True

opt = True
if opt and optimize_flag:
    top.driver = pyOptSparseDriver()
    # top.driver.options['optimizer'] = 'IPOPT'
    # top.driver.opt_settings['mu_strategy'] = 'adaptive'
    # top.driver.opt_settings['max_iter'] = 150
    # top.driver.opt_settings['nlp_scaling_method'] = 'none'
    # top.driver.opt_settings['print_level'] = 2

    # top.driver.opt_settings['file_print_level'] = 5
    # top.driver.opt_settings['nlp_scaling_method'] = 'none'
    # #
    # top.driver.opt_settings['derivative_test'] = 'first-order'
    # top.driver.opt_settings['point_perturbation_radius'] = .2
    # top.driver.opt_settings['max_iter'] = 10
    # top.driver.opt_settings['derivative_test_print_all'] = 'yes'
    # top.driver.opt_settings['findiff_perturbation'] = 1.e-2

    top.driver.options['optimizer'] = 'SNOPT'
    top.driver.opt_settings = {'Major step limit': 0.1,
    #                        #  'Major Print level':000011,
    #                        #  'Minor Print level':11,
    #                        # 'Difference interval':1.e-3,
    #                        #  'Print frequency': 1,
    #                        #  'Summary frequency':1,
    #                        #  'System information': 'Yes',
                             'Iterations limit': 100000,
                              'Major iterations limit': 50,
                              'Verify level': -1}
    top.driver.hist_file = 'pyopt_hist.dat'

top.root.fd_options['force_fd'] = True
top.root.fd_options['step_size'] = 2.e-2
# --- 3 -----

# basic geometry
root.add('hub_height_c', IndepVarComp('hub_height', 119., units='m'), promotes=['*'])
root.add('hub_radius_c', IndepVarComp('hub_radius', 2.8, units='m'), promotes=['*'])
root.add('hub_scale_c', IndepVarComp('blade_scaler', 0.), promotes=['*'])
# dependent variables
root.add('blade_scaler_c', ExecComp('blade_scale = 1. + blade_scaler * 0.01'), promotes=['*'])
root.add('blade_length_c', ExecComp('blade_length = 86.366 * blade_scale'), promotes=['*'])

root.add('tsr_scaler_c', IndepVarComp('tsr_scaler', 1.), promotes=['*'])
root.add('tsr_c', ExecComp('tsr = tsr_scaler * 7.5'), promotes=['*'])

# --- 3 -----

# Geometry

pf = read_blade_planform('data/DTU_10MW_RWT_blade_axis_prebend.dat')
nsec_ae = 30
nsec_st = 20
dist = np.array([[0., 1./nsec_ae, 1], [1., 1./nsec_ae/3., nsec_ae]])
from PGL.main.distfunc import distfunc
s_ae = distfunc(dist)
s_st = np.linspace(0, 1, nsec_st)
pf = redistribute_planform(pf, s=s_ae)

# add planform spline component
spl_ae = root.add('pf_splines', SplinedBladePlanform(pf), promotes=['*'])

# component for interpolating planform onto structural mesh
redist = root.add('pf_st', PGLRedistributedPlanform('_st', nsec_ae, s_st), promotes=['*'])

# configure blade surface for structural solver
cfg = {}
cfg['redistribute_flag'] = False
cfg['blend_var'] = np.array([0.241, 0.301, 0.36, 0.48, 0.72, 1.0])
afs = []
for f in ['data/ffaw3241.dat',
          'data/ffaw3301.dat',
          'data/ffaw3360.dat',
          'data/ffaw3480.dat',
          'data/tc72.dat',
          'data/cylinder.dat']:

    afs.append(np.loadtxt(f))
cfg['base_airfoils'] = afs
surf = root.add('blade_surf', PGLLoftedBladeSurface(cfg, size_in=nsec_st,
                                size_out=(200, nsec_st, 3), suffix='_st'), promotes=['*'])

# read the blade structure
st3d = read_bladestructure('data/DTU10MW')

# and interpolate onto new distribution
st3dn = interpolate_bladestructure(st3d, s_st)

spl_st = root.add('st_splines', SplinedBladeStructure(st3dn), promotes=['*'])

st_props = root.add('st_props', BladeStructureProperties((200, nsec_st, 3), st3dn, [4,5,8,9]), promotes=['*'])
# --- 4 -----

# inputs to CS2DtoBECAS and BECASWrapper
config = {}
cfg = {}
cfg['dry_run'] = dry_run
cfg['dominant_elsets'] = ['REGION04', 'REGION08']
cfg['max_layers'] = 8
config['CS2DtoBECAS'] = cfg
cfg = {}
cfg['hawc2_FPM'] = FPM
cfg['plot_paraview'] = False
cfg['dry_run'] = dry_run
cfg['analysis_mode'] = 'stiffness'
cfg['checkmesh'] = False
config['BECASWrapper'] = cfg

root.add('stiffness', BECASBeamStructure(root, config, st3dn, (200, nsec_st, 3)), promotes=['*'])

# --- 5 -----

# General options
config = {}
config['master_file'] = 'htc/master_h2_fpm.htc'

config['with_tsr'] = True
config['with_structure'] = True
config['with_geom'] = True
config['hawc2_FPM'] = FPM
config['aerodynamic_sections'] = nsec_ae
config['structural_sections'] = nsec_st

# HAWC2GeometryBuilder
cf = {}
cf['interp_from_htc'] = False
config['HAWC2GeometryBuilder'] = cf

# HAWC2Wrapper
cf = {}
cf['dry_run'] = False
cf['copyback_results'] = False
cf['hawc2bin'] = 'HAWC2mb.exe'
cf['verbose'] = True
cf['timeout'] = 200.
config['HAWC2Wrapper'] = cf

# HAWC2Outputs
cf = {}
cf['channels'] = ['Ae rot. power',
                  'DLL-5-inpvec-1']
nchannels = len(cf['channels'])
cf['ch_envelope'] = []
cf['nr_blades_out'] = 3

for nsec in range(1, config['structural_sections']+1):
    for nrb in range(1, cf['nr_blades_out']+1):
        cf['ch_envelope'].append(\
    ['blade%i-blade%i-node-%03d-momentvec-x' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-momentvec-y' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-momentvec-z' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-forcevec-x' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-forcevec-y' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-forcevec-z' % (nrb, nrb, nsec)])
cf['psf'] = {}
cf['dry_run'] = False

config['HAWC2Outputs'] = cf

# HAWC2InputWriter
cf = {}
config['HAWC2InputWriter'] = cf


root.add('loads', HAWC2AeroElasticSolver(config, 'DLCs_fpm', dlcs_fext='xls'), promotes=['*'])
ncases = root.loads.ncases


config = {}
cfg = {}
cfg['hawc2_FPM'] = FPM
cfg['plot_paraview'] = False
cfg['dry_run'] = False
cfg['analysis_mode'] = 'stress_recovery'
config['BECASWrapper'] = cfg

root.add('stress_recovery', BECASStressRecovery(config, s_st, 12), promotes=['*'])    
root.add('envelope_correction', CorrectedEnvelopes(nsec_st, 12,
                    'full_envelopes.sqlite', 'fpm_envelopes.sqlite'),promotes=['*'])
                    
for i in range(nsec_st):
    root.connect('blade_loads_envelope_sec%03d'%i,'blade_loads_envelope_sec%03d_ori'%i)
    root.connect('blade_loads_envelope_sec%03d_corr'%i, 'load_cases_sec%03d'%i)

# --- 6 -----

# AEP
wsp_array = [4,5,6,7,8,9,9.5,10,10.5,11,11.5,12,13,14,15,16,17,18,19,20,21,22,23,24,25]
nwsp = len(wsp_array)

root.add('h2_powercurve', H2PowerCurve(nwsp,nchannels,ncases,0), promotes=['P'])
root.connect('stat_mean', 'h2_powercurve.mean_stats')
root.add('aep_calc', WeibullAEP(nwsp, wsp_array, A=8., k=2.), promotes=['aep'])
root.connect('P', 'aep_calc.power_curve')

# --- 7 -----

factor = 0.925
mass_ref = 42576.43041308516
aep_ref = 33387400.2862
tow_cl_ref = 4.884186276
aep = ExecComp('objf = -(aep / %f)' % (aep_ref))

root.add('objf_c', aep, promotes=['*'])

root.add('loads_tower_clearance_c', ExecComp('tip_tower_clearance=numpy.min(stat_min[:,-1])/4.884186276',
                            stat_min=np.zeros((ncases, nchannels))), promotes=['*'])

root.add('massmom_con', ExecComp('mass_mom_con = blade_mass_moment / 12116942.225227021'), promotes=['*'])
root.add('mass_con', ExecComp('mass_con = blade_mass / 42537.655'), promotes=['*'])

if optimize_flag:

    # add blade structure splines
    spl_st.add_spline(('r04uniax00T', 'r04uniax01T',
                       'r08uniax00T', 'r08uniax01T'),
                       np.array([0., 0.2, 0.45, 0.75, 0.95, 1.]), spline_type='bezier', scaler=0.04)
    spl_st.add_spline(('r04uniax00A', 'r04uniax01A'),
                       np.array([0., 0.2, 0.45, 0.75, 0.95, 1.]), spline_type='bezier', scaler=10.)
    spl_st.add_spline(('r08uniax00A', 'r08uniax01A'),
                       np.array([0., 0.2, 0.45, 0.75, 0.95, 1.]), spline_type='bezier', scaler=10.)
    # 14 dvs
    top.driver.add_desvar('r04uniax00T_C', lower=-2., upper=2., indices=[1,2,3,4])
    top.driver.add_desvar('r04uniax00A_C', lower=-2., upper=2., indices=[1,2,3,4,5])
    top.driver.add_desvar('r08uniax00A_C', lower=-2., upper=2., indices=[1,2,3,4,5])

    # add blade planform splines
    spl_ae.add_spline('rot_z', np.array([0., 0.25, 0.45, 0.65, 0.9, 1]), spline_type='bezier', scaler=10.)
    spl_ae.add_spline('y', np.array([0., 0.25, 0.45, 0.65, 0.9, 1]), spline_type='bezier', scaler=.06)

    # add design variables
    # 11 dvs
    top.driver.add_desvar('rot_z_C', lower=-1, upper=1, indices=[1,2,3,4,5])
    top.driver.add_desvar('y_C', lower=-1.2, upper=1.2, indices=[2,3,4,5])
    top.driver.add_desvar('tsr_scaler', lower=0.8, upper=1.5)
    top.driver.add_desvar('blade_scaler', lower=-10, upper=20.)

    root.add('pb_con', ExecComp('pb_con = -numpy.min(y) * blade_scale / 0.072', y=np.zeros(30)), promotes=['*'])
    
    # add constraints    
    top.driver.add_constraint('y_curv', upper=0.)
    top.driver.add_constraint('tip_tower_clearance', lower=1.)
    top.driver.add_constraint('pb_con', upper=1.)
    top.driver.add_constraint('mass_mom_con', upper=1.005)
    top.driver.add_constraint('mass_con', upper=1.005)

    top.driver.add_constraint('r04uniax00T', lower=4.e-4)
    top.driver.add_constraint('r08uniax00T', lower=4.e-4)
    
    # blade failure
    top.driver.add_constraint('blade_failure_index_ks', upper=1.)

    top.driver.add_objective('objf')

spl_st.configure()
spl_ae.configure()


# convenience functions to generate list of varnames to record
pf_vars = get_planform_recording_vars(with_CPs=True)
pf_stvars = get_planform_recording_vars(suffix='_st')
st_vars = get_structure_recording_vars(st3dn, with_props=True)

# add the recorder
from openmdao.api import SqliteRecorder, DumpRecorder
recorder = SqliteRecorder('optimization.sqlite')

recorder.options['includes'] = ['s', 's_st','blade_beam_structure',
                               'blade_mass', 'blade_mass_moment', 'aep',
                               'blade_scale', 'blade_length', 'objf','pb_con']
recorder.options['includes'] += ['mass_con', 'mass_mom_con']
recorder.options['includes'] += ['r04uniax00A', 'r08uniax00A']
recorder.options['includes'] += pf_vars
recorder.options['includes'] += pf_stvars
recorder.options['includes'] += st_vars
recorder.options['includes'] += top.root.loads.aggregate.stat_var
recorder.options['includes'] += top.root.loads.aggregate.envelope_var
recorder.options['includes'] += ['blade_failure_index', 'blade_failure_index_ks',
                                 'tip_tower_clearance']

top.driver.add_recorder(recorder)

t00 = time.time()
top.setup()
print('Setup time', time.time()-t00)

t0 = time.time()
top.run()
print('Total Execution time', time.time()-t0)

# --- 8 -----
