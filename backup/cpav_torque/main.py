# -*- coding: utf-8 -*-
import unittest

from openmdao.core.mpi_wrap import MPI
from openmdao.api import Problem, Group, ParallelGroup
from openmdao.solvers.ln_gauss_seidel import LinearGaussSeidel
import numpy as np
from hawc2_wrapper.hawc2_aeroelasticsolver import HAWC2AeroElasticSolver

if MPI:
    from openmdao.core.petsc_impl import PetscImpl as impl
else:
    from openmdao.core.basic_impl import BasicImpl as impl


top = Problem(impl=impl, root=Group())
root = top.root
root.ln_solver = LinearGaussSeidel()
#top.setup(check=False)

config = {}

# HAWC2Wrapper
# General options
config['master_file'] = 'htc/master_h2_fpm.htc'

config['with_tsr'] = False
config['with_structure'] = False
config['hawc2_FPM'] = True
config['with_geom'] = False
config['aerodynamic_sections'] = 30
config['structural_sections'] = 20
# HAWC2GeometryBuilder
cf = {}
cf['interp_from_htc'] = False
config['HAWC2GeometryBuilder'] = cf
cf = {}
cf['copyback_results'] = False
cf['hawc2bin'] = 'hawc2mb.exe'
cf['wine_cmd'] = 'wine'
cf['verbose'] = True
cf['timeout'] = 2000
config['HAWC2Wrapper'] = cf

# HAWC2Outputs
cf = {}
# cf['dry_run'] = True
cf['channels'] = ['Ae rot. power','DLL-5-inpvec-1']
# cf['channels'] = ['tower-tower-node-001-momentvec-x',
#                   'tower-tower-node-001-momentvec-y',
#                   'blade1-blade1-node-003-momentvec-x',
#                   'blade1-blade1-node-003-momentvec-y',
#                   'bearing-shaft_rot-angle_speed-rpm',
#                   'bearing-pitch1-angle-deg']
cf['ch_envelope'] = [] 
cf['nr_blades_out'] = 3

for nsec in range(1, config['structural_sections']+1):
    for nrb in range(1, cf['nr_blades_out']+1):
        cf['ch_envelope'].append(\
    ['blade%i-blade%i-node-%03d-momentvec-x' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-momentvec-y' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-momentvec-z' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-forcevec-x' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-forcevec-y' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-forcevec-z' % (nrb, nrb, nsec)])
cf['psf'] = {}
config['HAWC2Outputs'] = cf

# HAWC2InputWriter
cf = {}
cf['turb_directory'] = './turb'
config['HAWC2InputWriter'] = cf


root.add('loads', HAWC2AeroElasticSolver(config, 'DLCs_fpm', dlcs_fext='xls'), promotes=['*'])

# add the recorder
from openmdao.api import SqliteRecorder, DumpRecorder
recorder = SqliteRecorder('optimization.sqlite')

recorder.options['includes'] += top.root.loads.aggregate.stat_var
#recorder.options['includes'] += top.root.loads.aggregate.fatigue_var
recorder.options['includes'] += top.root.loads.aggregate.envelope_var
#recorder.options['includes'] += ['blade_failure_index_sec%03d'%i for i in range(nsec_st)]

top.driver.add_recorder(recorder)

top.setup()

top.run()

