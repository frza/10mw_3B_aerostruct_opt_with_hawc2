
# --- 1 ---

import numpy as np
import time
import os

from openmdao.core.mpi_wrap import MPI
from openmdao.api import Problem, Group, IndepVarComp, ParallelFDGroup, Component
from openmdao.api import ScipyOptimizer, ExecComp
from openmdao.solvers.ln_gauss_seidel import LinearGaussSeidel

if MPI:
    from openmdao.core.petsc_impl import PetscImpl as impl
else:
    from openmdao.core.basic_impl import BasicImpl as impl

from openmdao.drivers.pyoptsparse_driver import pyOptSparseDriver


from fusedwind.turbine.geometry import read_blade_planform,\
                                       redistribute_planform,\
                                       PGLLoftedBladeSurface,\
                                       SplinedBladePlanform, \
                                       PGLRedistributedPlanform
from fusedwind.turbine.structure import read_bladestructure, \
                                        write_bladestructure, \
                                        interpolate_bladestructure, \
                                        SplinedBladeStructure, \
                                        BladeStructureProperties
from fusedwind.turbine.recorders import get_planform_recording_vars, \
                                        get_structure_recording_vars

# from hawtopt2.main import HawtOpt2Main
from becas_wrapper.becas_bladestructure import BECASBeamStructure
from becas_wrapper.becas_stressrecovery import BECASStressRecovery
from hawc2_wrapper.hawc2s_aeroelasticsolver import HAWC2SAeroElasticSolver
from hawc2_wrapper.hawc2_aeroelasticsolver import HAWC2AeroElasticSolver

from sqlitedict import SqliteDict

user_home = os.getenv('HOME')


class WeibullAEP(Component):

    def __init__(self, size, wsp_array, A=8., k=2.):
        super(WeibullAEP, self).__init__()

        self.wsp_array = wsp_array
        self.A = A
        self.k = k
        self.exclude = []
        self.wspIn = wsp_array[0]
        self.wspOut = wsp_array[-1]

        self.add_param('power_curve', np.zeros(size))
        self.add_output('aep', 0.)

    def solve_nonlinear(self, params, unknowns, resids):

        cdf_ws = lambda A,k,u, bin=1.0: np.exp(-(max(0.0, u-bin/2.0)/A)**k) - np.exp(-((u+bin/2.0)/A)**k)

        ws_f = []
        ps = []
        wsps = []
        for i, ws in enumerate(self.wsp_array):
            if i in self.exclude: continue
            if ws > self.wspOut: continue
            if ws < self.wspIn: continue
            ws_f.append(cdf_ws(self.A, self.k, ws))
            ps.append(params['power_curve'][i])
            wsps.append(ws)
        self.ws_f = np.asarray(ws_f)
        ps = np.asarray(ps)
        wsps = np.asarray(wsps)

        unknowns['aep'] = np.trapz(ps * self.ws_f * 8760.0, wsps)
        
class H2RotorOutput(Component):

    def __init__(self, nwsp, nchannels, ncases, ch_list):
        super(H2RotorOutput, self).__init__()

        self.nwsp = nwsp
        self.ch_list = ch_list

        self.add_param('mean_stats', shape=[ncases,nchannels])
        self.add_output('P', np.zeros(nwsp))
        self.add_output('T', np.zeros(nwsp))
        self.add_output('Q', np.zeros(nwsp))
        self.add_output('RPM', np.zeros(nwsp))

    def solve_nonlinear(self, params, unknowns, resids):

        unknowns['P'] = params['mean_stats'][:self.nwsp,self.ch_list[0]]
        unknowns['T'] = params['mean_stats'][:self.nwsp,self.ch_list[1]]
        unknowns['Q'] = params['mean_stats'][:self.nwsp,self.ch_list[2]]
        unknowns['RPM'] = params['mean_stats'][:self.nwsp,self.ch_list[3]]

class H2BladeAero(Component):

    def __init__(self, nwsp, nchannels, ncases, ch_list):
        super(H2BladeAero, self).__init__()

        self.nwsp = nwsp
        self.ch_list = ch_list

        self.add_param('mean_stats', shape=[ncases,nchannels])
        self.add_output('Cl', np.zeros((nwsp,len(ch_list))))

    def solve_nonlinear(self, params, unknowns, resids):

        unknowns['Cl'] = params['mean_stats'][:self.nwsp,self.ch_list[0]:self.ch_list[-1]+1]
        
class CorrectedEnvelopes(Component):

    def __init__(self, nsec, nsectors, full_dict, red_dict):
        super(CorrectedEnvelopes, self).__init__()

        self.full_dict = full_dict
        self.red_dict = red_dict
        self.nsec = nsec
        
        self.envelope_corr_var = []
        
        db_full = SqliteDict(self.full_dict, 'openmdao' )
        self.us_full = db_full['rank0:Driver/1']['Unknowns']

        db_red = SqliteDict(self.red_dict, 'openmdao' )
        self.us_red = db_red['rank0:Driver/1']['Unknowns']
        
        for isec in range(nsec):
            p_name = 'blade_loads_envelope_sec%03d_ori' %isec
            self.add_param(p_name, shape=[nsectors, 6])
            name = 'blade_loads_envelope_sec%03d_corr' %isec
            self.envelope_corr_var.append(name)
            self.add_output(name, shape=[nsectors, 6])

    def solve_nonlinear(self, params, unknowns, resids):
              
        for i, var in enumerate(self.envelope_corr_var):
            theta = params['blade_loads_envelope_sec%03d_ori'%i]\
                           /self.us_red['blade_loads_envelope_sec%03d'%i]\
                           *self.us_full['blade_loads_envelope_sec%03d'%i]
            unknowns[var] = np.nan_to_num(theta)


optimize_flag = True
grad = False
dry_run = False

FPM = True
par_fd = 33

if optimize_flag:
    top = Problem(impl=impl, root=ParallelFDGroup(par_fd))
else:
    top = Problem(impl=impl, root=Group())

root = top.root
root.ln_solver = LinearGaussSeidel()
root.ln_solver.options['single_voi_relevance_reduction'] = True

opt = True
if opt and optimize_flag:
    top.driver = pyOptSparseDriver()
    # top.driver.options['optimizer'] = 'IPOPT'
    # top.driver.opt_settings['mu_strategy'] = 'adaptive'
    # top.driver.opt_settings['max_iter'] = 150
    # top.driver.opt_settings['nlp_scaling_method'] = 'none'
    # top.driver.opt_settings['print_level'] = 2

    # top.driver.opt_settings['file_print_level'] = 5
    # top.driver.opt_settings['nlp_scaling_method'] = 'none'
    # #
    # top.driver.opt_settings['derivative_test'] = 'first-order'
    # top.driver.opt_settings['point_perturbation_radius'] = .2
    # top.driver.opt_settings['max_iter'] = 10
    # top.driver.opt_settings['derivative_test_print_all'] = 'yes'
    # top.driver.opt_settings['findiff_perturbation'] = 1.e-2

    top.driver.options['optimizer'] = 'SNOPT'
    top.driver.opt_settings = {'Major step limit': 0.1,
    #                        #  'Major Print level':000011,
    #                        #  'Minor Print level':11,
    #                        # 'Difference interval':1.e-3,
    #                        #  'Print frequency': 1,
    #                        #  'Summary frequency':1,
    #                        #  'System information': 'Yes',
                             'Iterations limit': 100000,
                              'Major iterations limit': 150,
                              'Verify level': -1}
    top.driver.hist_file = 'pyopt_hist.dat'

top.root.fd_options['force_fd'] = True
top.root.fd_options['step_size'] = 2.e-2

# --- 2 ---

# basic geometry
root.add('hub_height_c', IndepVarComp('hub_height', 119., units='m'), promotes=['*'])
root.add('cone_c', IndepVarComp('cone_angle', 2.5, units='m'), promotes=['*'])
root.add('hub_radius_c', IndepVarComp('hub_radius', 2.8, units='m'), promotes=['*'])
root.add('hub_scale_c', IndepVarComp('blade_scaler', 0.), promotes=['*'])
# dependent variables
root.add('blade_scaler_c', ExecComp('blade_scale = 1. + blade_scaler * 0.01'), promotes=['*'])
root.add('blade_length_c', ExecComp('blade_length = 86.366 * blade_scale'), promotes=['*'])

root.add('tsr_scaler_c', IndepVarComp('tsr_scaler', 1.), promotes=['*'])
root.add('tsr_c', ExecComp('tsr = tsr_scaler * 7.5'), promotes=['*'])

# --- 3 ---

# Geometry

pf = read_blade_planform('data/DTU_10MW_RWT_blade_axis_prebend.dat')
nsec_ae = 30
nsec_st = 19
dist = np.array([[0., 1./nsec_ae, 1], [1., 1./nsec_ae/3., nsec_ae]])
from PGL.main.distfunc import distfunc
s_ae = distfunc(dist)
s_st = np.linspace(0, 1, nsec_st)
pf = redistribute_planform(pf, s=s_ae)

# add planform spline component
spl_ae = root.add('pf_splines', SplinedBladePlanform(pf), promotes=['*'])

# component for interpolating planform onto structural mesh
redist = root.add('pf_st', PGLRedistributedPlanform('_st', nsec_ae, s_st), promotes=['*'])

# configure blade surface for structural solver
cfg = {}
cfg['redistribute_flag'] = False
cfg['blend_var'] = np.array([0.241, 0.301, 0.36, 0.48, 0.72, 1.0])
afs = []
for f in ['data/ffaw3241.dat',
          'data/ffaw3301.dat',
          'data/ffaw3360.dat',
          'data/ffaw3480.dat',
          'data/tc72.dat',
          'data/cylinder.dat']:

    afs.append(np.loadtxt(f))
cfg['base_airfoils'] = afs
surf = root.add('blade_surf', PGLLoftedBladeSurface(cfg, size_in=nsec_st,
                                size_out=(200, nsec_st, 3), suffix='_st'), promotes=['*'])

# read the blade structure
st3d = read_bladestructure('data/DTU10MW')

# and interpolate onto new distribution
st3dn = interpolate_bladestructure(st3d, s_st)

spl_st = root.add('st_splines', SplinedBladeStructure(st3dn), promotes=['*'])

st_props = root.add('st_props', BladeStructureProperties((200, nsec_st, 3), st3dn, [4,5,8,9]), promotes=['*'])

# --- 4 ---

# inputs to CS2DtoBECAS and BECASWrapper
config = {}
cfg = {}
cfg['dry_run'] = dry_run
cfg['dominant_elsets'] = ['REGION04', 'REGION08']
cfg['max_layers'] = 8
config['CS2DtoBECAS'] = cfg
cfg = {}
cfg['hawc2_FPM'] = FPM
cfg['plot_paraview'] = False
cfg['dry_run'] = dry_run
cfg['analysis_mode'] = 'stiffness'
cfg['checkmesh'] = False
config['BECASWrapper'] = cfg

root.add('stiffness', BECASBeamStructure(root, config, st3dn, (200, nsec_st, 3)), promotes=['*'])

# --- 5 ---

# General options
config = {}
config['master_file'] = 'htc/master_h2_fpm_simple_19.htc'

config['with_tsr'] = True
config['with_cone'] = True
config['with_structure'] = True
config['with_geom'] = True
config['hawc2_FPM'] = FPM
config['aerodynamic_sections'] = nsec_ae
config['structural_sections'] = nsec_st

# HAWC2GeometryBuilder
cf = {}
cf['interp_from_htc'] = False
config['HAWC2GeometryBuilder'] = cf

# HAWC2Wrapper
cf = {}
cf['dry_run'] = False
cf['copyback_results'] = False
cf['hawc2bin'] = 'HAWC2mb.exe'
cf['verbose'] = True
cf['timeout'] = 3000.
config['HAWC2Wrapper'] = cf

# HAWC2Outputs
cf = {}
cf['channels'] = ['Ae rot. power',
                  'Ae rot. thrust',
                  'Ae rot. torque',
                  'bearing-shaft_rot-angle_speed-rpm',
                  'Cl-1-35.0','Cl-1-40.0','Cl-1-45.0','Cl-1-50.0','Cl-1-55.0',
                  'Cl-1-60.0','Cl-1-65.0','Cl-1-70.0','Cl-1-75.0','Cl-1-80.0',
                  'tower-tower-node-001-momentvec-x',
                  'tower-tower-node-001-momentvec-y',
                  'tower-tower-node-011-momentvec-x',
                  'tower-tower-node-011-momentvec-y',
                  'DLL-5-inpvec-1']
nchannels = len(cf['channels'])
cf['ch_envelope'] = []
cf['nr_blades_out'] = 3
cf['Nsectors'] = 12
cf['Nx'] = 36

for nsec in range(1, config['structural_sections']+1):
    for nrb in range(1, cf['nr_blades_out']+1):
        cf['ch_envelope'].append(\
    ['blade%i-blade%i-node-%03d-momentvec-x' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-momentvec-y' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-momentvec-z' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-forcevec-x' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-forcevec-y' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-forcevec-z' % (nrb, nrb, nsec)])
cf['psf'] = {}
cf['dry_run'] = False

config['HAWC2Outputs'] = cf

# HAWC2InputWriter
cf = {}
config['HAWC2InputWriter'] = cf


root.add('loads', HAWC2AeroElasticSolver(config, 'DLCs_simple', dlcs_fext='xls'), promotes=['*'])
ncases = root.loads.ncases


config = {}
cfg = {}
cfg['hawc2_FPM'] = FPM
cfg['plot_paraview'] = False
cfg['dry_run'] = False
cfg['analysis_mode'] = 'stress_recovery'
config['BECASWrapper'] = cfg

root.add('stress_recovery', BECASStressRecovery(config, s_st, 12), promotes=['*'])    
root.add('envelope_correction', CorrectedEnvelopes(nsec_st, 12,
                    'full_envelopes.sqlite', 'simple_envelopes.sqlite'),promotes=['*'])
                    
for i in range(nsec_st):
    root.connect('blade_loads_envelope_sec%03d'%i,'blade_loads_envelope_sec%03d_ori'%i)
    root.connect('blade_loads_envelope_sec%03d_corr'%i, 'load_cases_sec%03d'%i)

# --- 6 ---

# AEP
wsp_array = [4,5,6,7,8,9,9.5,10,10.5,11,12,13,14,16,18,20,22,25]
nwsp = len(wsp_array)

root.add('h2_rotoroutput', H2RotorOutput(nwsp,nchannels,ncases,[0,1,2,3]), promotes=['*'])
root.connect('stat_mean', 'mean_stats')
root.add('aep_calc', WeibullAEP(nwsp, wsp_array, A=8., k=2.), promotes=['aep'])
root.connect('P', 'aep_calc.power_curve')

root.add('h2_aerooutput', H2BladeAero(nwsp,nchannels,ncases,[4,5,6,7,8,9,10,11,12,13]), promotes=['*'])

# --- 7 ---

#---------------------
#--- COST FUNCTION ---
#---------------------

# read reference values
db = SqliteDict('simple_envelopes.sqlite', 'openmdao' )
us = db['rank0:Driver/1']['Unknowns']

T_ref = np.max(us['stat_mean'][:len(wsp_array),1])
Q_ref = np.max(us['stat_mean'][:len(wsp_array),2])

MxTB_max_ref = np.max(us['stat_max'][:,14])
MxTB_min_ref = np.min(us['stat_min'][:,14])
MxTT_max_ref = np.max(us['stat_max'][:,16])
MxTT_min_ref = np.min(us['stat_min'][:,16])

aep = ExecComp('objf = -(aep/%f)' % (us['aep']))

root.add('objf_c', aep, promotes=['*'])

#--------------------------    
#--- GENERAL PARAMETERS ---
#--------------------------

# Structure
root.add('massmom_con', ExecComp('mass_mom_con = blade_mass_moment /%f' % (us['blade_mass_moment']*1.05)), promotes=['*'])
root.add('mass_con', ExecComp('mass_con = blade_mass /%f' % (us['blade_mass']*1.05)), promotes=['*'])

if optimize_flag:
    root.add('W08_c', IndepVarComp('width_08_ref', us['r08_width']*us['blade_length']), promotes=['*'])
    root.add('W04_c', IndepVarComp('width_04_ref', us['r04_width']*us['blade_length']), promotes=['*'])
    root.add('TWu_c', IndepVarComp('TWu', np.minimum(us['r08_thickness']/(us['r08_width']*us['blade_length']), 0.08)), promotes=['*'])
    root.add('TWl_c', IndepVarComp('TWl', np.minimum(us['r04_thickness']/(us['r04_width']*us['blade_length']), 0.08)), promotes=['*'])
    root.add('TWconL_c', ExecComp('TWconL=r04_thickness/(r04_width*blade_length)-TWl', r04_thickness=np.zeros(nsec_st),
	                                                                           r04_width=np.zeros(nsec_st),
	                                                                           TWl=np.zeros(nsec_st),
	                                                                           TWconL=np.zeros(nsec_st)), promotes=['*'])
    root.add('TWconU_c', ExecComp('TWconU=r08_thickness/(r08_width*blade_length)-TWu', r08_thickness=np.zeros(nsec_st),
	                                                                           r08_width=np.zeros(nsec_st),
	                                                                           TWu=np.zeros(nsec_st),
	                                                                           TWconU=np.zeros(nsec_st)), promotes=['*'])
    root.add('web_diff02_c', ExecComp('web_angle_diff=web_angle02-web_angle03', web_angle02=np.zeros(nsec_st),
	                                                                    web_angle03=np.zeros(nsec_st),
	                                                                    web_angle_diff=np.zeros(nsec_st)), promotes=['*'])
    root.add('pacc_c', ExecComp('pacc=0.5*(pacc_u[:,0]+pacc_l[:,0])', pacc_u=np.zeros((nsec_st,2)), pacc_l=np.zeros((nsec_st,2)),pacc=np.zeros(nsec_st)), promotes=['*'])
    root.add('r08_width_con_c', ExecComp('r08_width_con=r08_width*blade_length/width_08_ref', r08_width=np.zeros(nsec_st), width_08_ref=np.zeros(nsec_st),r08_width_con=np.zeros(nsec_st)), promotes=['*'])
    root.add('r04_width_con_c', ExecComp('r04_width_con=r04_width*blade_length/width_04_ref', r04_width=np.zeros(nsec_st), width_04_ref=np.zeros(nsec_st),r04_width_con=np.zeros(nsec_st)), promotes=['*'])
    
    # Aerodynamics
    root.add('cl_oper1_c', ExecComp('cl_oper1=Cl[:,0]/1.35', cl_oper1=np.zeros(nwsp),Cl=np.zeros((nwsp,10))), promotes=['*'])
    root.add('cl_oper2_c', ExecComp('cl_oper2=Cl[:,1]/1.35', cl_oper2=np.zeros(nwsp),Cl=np.zeros((nwsp,10))), promotes=['*'])
    root.add('cl_oper3_c', ExecComp('cl_oper3=Cl[:,2]/1.35', cl_oper3=np.zeros(nwsp),Cl=np.zeros((nwsp,10))), promotes=['*'])
    root.add('cl_oper4_c', ExecComp('cl_oper4=Cl[:,3]/1.35', cl_oper4=np.zeros(nwsp),Cl=np.zeros((nwsp,10))), promotes=['*'])
    root.add('cl_oper5_c', ExecComp('cl_oper5=Cl[:,4]/1.35', cl_oper5=np.zeros(nwsp),Cl=np.zeros((nwsp,10))), promotes=['*'])
    root.add('cl_oper6_c', ExecComp('cl_oper6=Cl[:,5]/1.35', cl_oper6=np.zeros(nwsp),Cl=np.zeros((nwsp,10))), promotes=['*'])
    root.add('cl_oper7_c', ExecComp('cl_oper7=Cl[:,6]/1.35', cl_oper7=np.zeros(nwsp),Cl=np.zeros((nwsp,10))), promotes=['*'])
    root.add('cl_oper8_c', ExecComp('cl_oper8=Cl[:,7]/1.4', cl_oper8=np.zeros(nwsp),Cl=np.zeros((nwsp,10))), promotes=['*'])
    root.add('cl_oper9_c', ExecComp('cl_oper9=Cl[:,8]/1.4', cl_oper9=np.zeros(nwsp),Cl=np.zeros((nwsp,10))), promotes=['*'])
    root.add('cl_oper10_c', ExecComp('cl_oper10=Cl[:,9]/1.45', cl_oper10=np.zeros(nwsp),Cl=np.zeros((nwsp,10))), promotes=['*'])
	
    # Aeroelastic
    root.add('loads_tower_clearance_c', ExecComp('tip_tower_clearance=numpy.min(stat_min[:18,-1])',
                                             stat_min=np.zeros((ncases, nchannels))), promotes=['*'])

    root.add('T_norm', ExecComp('T_con=T/%f' % (T_ref*1.05), T_con=np.zeros(nwsp), T=np.zeros(nwsp)), promotes=['*'])
    root.add('Q_norm', ExecComp('Q_con=Q/%f' % (Q_ref*1.05), Q_con=np.zeros(nwsp), Q=np.zeros(nwsp)), promotes=['*'])
    root.add('RPM_norm', ExecComp('RPM_con=RPM/(9.6*1.15)', RPM_con=np.zeros(nwsp), RPM=np.zeros(nwsp)), promotes=['*'])                                                                                     
                                                                                  
    root.add('MxTB_max_c', ExecComp('MxTB_max=numpy.max(stat_max[:,14])/%f' % (MxTB_max_ref*1.05), stat_max=np.zeros((ncases, nchannels))), promotes=['*'])
    root.add('MxTB_min_c', ExecComp('MxTB_min=numpy.min(stat_min[:,14])/%f' % (MxTB_min_ref*1.05), stat_min=np.zeros((ncases, nchannels))), promotes=['*'])
    root.add('MxTT_max_c', ExecComp('MxTT_max=numpy.max(stat_max[:,16])/%f' % (MxTT_max_ref*1.05), stat_max=np.zeros((ncases, nchannels))), promotes=['*'])
    root.add('MxTT_min_c', ExecComp('MxTT_min=numpy.min(stat_min[:,16])/%f' % (MxTT_min_ref*1.05), stat_min=np.zeros((ncases, nchannels))), promotes=['*'])

#------------------------
#--- DESIGN VARIABLES ---
#------------------------
    
#--- DVs Planform xp ---    
    
    # add blade planform splines
    spl_ae.add_spline('chord', np.array([0., 0.25, 0.45, 0.65, 0.9, 1]), spline_type='bezier', scaler=0.1)
    spl_ae.add_spline('rthick', np.array([0., 0.25, 0.6, 0.85, 1]), spline_type='bezier', scaler=1.)
    spl_ae.add_spline('rot_z', np.array([0., 0.25, 0.45, 0.65, 0.9, 1]), spline_type='bezier', scaler=10.)
    spl_ae.add_spline('y', np.array([0., 0.25, 0.45, 0.65, 0.9, 1]), spline_type='bezier', scaler=.06)
    spl_ae.add_spline('x', np.array([0., 0.25, 0.45, 0.65, 0.9, 1]), spline_type='bezier', scaler=.06)

    # add design variables
    # 25 dvs	
    top.driver.add_desvar('tsr_scaler', lower=0.8, upper=1.5)
    top.driver.add_desvar('blade_scaler', lower=-10, upper=20.)
    top.driver.add_desvar('cone_angle', lower=0.0, upper=5.)
 
    top.driver.add_desvar('chord_C', lower=-1, upper=1, indices=[0,1,2,3,4])
    top.driver.add_desvar('rthick_C', lower=-1., upper=1., indices=[1,2,3,4])
    top.driver.add_desvar('rot_z_C', lower=-1, upper=1, indices=[0,1,2,3,4,5])
    top.driver.add_desvar('y_C', lower=-1.2, upper=1.2, indices=[2,3,4,5])
    top.driver.add_desvar('x_C', lower=-1.5, upper=1.5, indices=[3,4,5])

#--- DVs Structural xs ---

    # add blade structure splines
    spl_st.add_spline('DP04', np.array([0., 0.2, 0.5, 0.85, 1.]), spline_type='bezier', scaler=1.)
    spl_st.add_spline('DP05', np.array([0., 0.2, 0.5, 0.85, 1.]), spline_type='bezier', scaler=1.)
    spl_st.add_spline('DP08', np.array([0., 0.2, 0.5, 0.85, 1.]), spline_type='bezier', scaler=1.)
    spl_st.add_spline('DP09', np.array([0., 0.2, 0.5, 0.85, 1.]), spline_type='bezier', scaler=1.)
    spl_st.add_spline(('r04uniax00T', 'r04uniax01T',
                       'r08uniax00T', 'r08uniax01T'),
                       np.array([0., 0.2, 0.5, 0.85, 1.]), spline_type='bezier', scaler=0.04)
    spl_st.add_spline(('r00uniax00T', 'r00uniax01T',
                       'r01uniax00T', 'r01uniax01T',
                       'r11uniax00T', 'r11uniax01T',
                       'r12uniax00T', 'r12uniax01T',
                       'w00uniax00T', 'w00uniax01T'),
                       np.array([0., 0.2, 0.5, 0.85, 1.]), spline_type='bezier', scaler=0.004)
    spl_st.add_spline(('r00triax00T', 'r00triax01T',
                       'r01triax00T', 'r01triax01T',
                       'r11triax00T', 'r11triax01T',
                       'r12triax00T', 'r12triax01T',
                       'w00triax00T', 'w00triax01T'),
                       np.array([0., 0.2, 0.5, 0.85, 1.]), spline_type='bezier', scaler=0.001)
    spl_st.add_spline(('r02triax00T', 'r02triax01T',
                       'r03triax00T', 'r03triax01T',
                       'r09triax00T', 'r09triax01T',
                       'r10triax00T', 'r10triax01T'),
                       np.array([0., 0.2, 0.5, 0.85, 1.]), spline_type='bezier', scaler=0.002)
    spl_st.add_spline(('r05triax00T', 'r05triax01T',
                       'r06triax00T', 'r06triax01T',
                       'r07triax00T', 'r07triax01T'),
                       np.array([0., 0.2, 0.5, 0.85, 1.]), spline_type='bezier', scaler=0.002)
    spl_st.add_spline(('r06uniax00T', 'r06uniax01T'),
                       np.array([0., 0.2, 0.5, 0.85, 1.]), spline_type='bezier', scaler=0.002)
    spl_st.add_spline(('r04uniax00A', 'r04uniax01A'),
                       np.array([0., 0.2, 0.5, 0.85, 1.]), spline_type='bezier', scaler=10.)
    spl_st.add_spline(('r08uniax00A', 'r08uniax01A'),
                       np.array([0., 0.2, 0.5, 0.85, 1.]), spline_type='bezier', scaler=10.)
    # add design variables
    # 41 dvs
    top.driver.add_desvar('DP04_C', lower=-.5, upper=0.5, indices=[0,1,2,3])
    top.driver.add_desvar('DP05_C', lower=-.5, upper=0.5, indices=[0,1,2,3])
    top.driver.add_desvar('DP08_C', lower=-.5, upper=0.5, indices=[0,1,2,3])
    top.driver.add_desvar('DP09_C', lower=-.5, upper=0.5, indices=[0,1,2,3])
    top.driver.add_desvar('r04uniax00T_C', lower=-1., upper=1., indices=[0,1,2,3])
    top.driver.add_desvar('r00uniax00T_C', lower=-1., upper=1., indices=[0,1])
    top.driver.add_desvar('r00triax00T_C', lower=-1., upper=1., indices=[0,1])
    top.driver.add_desvar('r02triax00T_C', lower=-1., upper=1., indices=[0,1])
    top.driver.add_desvar('r05triax00T_C', lower=-1., upper=1., indices=[0,1,2,3])
    top.driver.add_desvar('r06uniax00T_C', lower=-1., upper=1., indices=[0,1,2])

    top.driver.add_desvar('r04uniax00A_C', lower=-1., upper=1., indices=[1,2,3,4])
    top.driver.add_desvar('r08uniax00A_C', lower=-1., upper=1., indices=[1,2,3,4])

#-------------------
#--- CONSTRAINTS ---
#-------------------

#--- Planform g ---

    root.add('chord_conc', ExecComp('maxchord_con = chord * blade_scale', chord=np.zeros(30), maxchord_con=np.zeros(30)), promotes=['*'])
    root.add('maxsweep_conc', ExecComp('maxsweep_con = numpy.max(numpy.abs(x*blade_length))', x=np.zeros(30)), promotes=['*'])    
    root.add('pb_conc', ExecComp('pb_con = -numpy.min(y) * blade_scale / 0.072', y=np.zeros(30)), promotes=['*'])
    top.driver.add_constraint('maxchord_con', upper=0.072)	
    top.driver.add_constraint('y_curv', upper=0.)
    top.driver.add_constraint('rthick', lower=0.241)
    top.driver.add_constraint('pb_con', upper=1.)
    top.driver.add_constraint('maxsweep_con', upper=3.1)

#--- Structural hg ---

    top.driver.add_constraint('mass_mom_con', upper=1.0)
    top.driver.add_constraint('mass_con', upper=1.0)
	
    top.driver.add_constraint('TWconU', lower=0.)
    top.driver.add_constraint('TWconL', lower=0.)
    top.driver.add_constraint('r04_width_con', lower=0.8)
    top.driver.add_constraint('r08_width_con', lower=0.8)
    top.driver.add_constraint('web_angle_diff', lower=-0.5, upper=0.5)
    top.driver.add_constraint('web_angle02', lower=-25., upper=1.)
    top.driver.add_constraint('pacc', lower=-0.0001, upper=0.0001, indices=[0])

    top.driver.add_constraint('r00triax00T', lower=7.5e-4)
    top.driver.add_constraint('r01triax00T', lower=7.5e-4)
    top.driver.add_constraint('r02triax00T', lower=7.5e-4)
    top.driver.add_constraint('r03triax00T', lower=7.5e-4)
    top.driver.add_constraint('r05triax00T', lower=7.5e-4)
    top.driver.add_constraint('r06triax00T', lower=7.5e-4)
    top.driver.add_constraint('r07triax00T', lower=7.5e-4)
    top.driver.add_constraint('r09triax00T', lower=7.5e-4)
    top.driver.add_constraint('r10triax00T', lower=7.5e-4)
    top.driver.add_constraint('r11triax00T', lower=7.5e-4)
    top.driver.add_constraint('r12triax00T', lower=7.5e-4)

    top.driver.add_constraint('r04uniax00T', lower=4.e-4)
    top.driver.add_constraint('r08uniax00T', lower=4.e-4)

    top.driver.add_constraint('r06uniax00T', lower=4.e-4, indices=[0,1,2,3,4,5,6,7,8,9])

    top.driver.add_constraint('r00uniax00T', lower=4.e-4, indices=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17])
    top.driver.add_constraint('r01uniax00T', lower=4.e-4, indices=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17])
    top.driver.add_constraint('r11uniax00T', lower=4.e-4, indices=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17])
    top.driver.add_constraint('r12uniax00T', lower=4.e-4, indices=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17])
    
#--- Strength hs ---
    # blade failure
    top.driver.add_constraint('blade_failure_index_ks', upper=1.)

#--- Aeroelastic k ---
   
    top.driver.add_constraint('tip_tower_clearance', lower=0.65)
    
    top.driver.add_constraint('cl_oper1', upper=1.)
    top.driver.add_constraint('cl_oper2', upper=1.)
    top.driver.add_constraint('cl_oper3', upper=1.)
    top.driver.add_constraint('cl_oper4', upper=1.)
    top.driver.add_constraint('cl_oper5', upper=1.)
    top.driver.add_constraint('cl_oper6', upper=1.)
    top.driver.add_constraint('cl_oper7', upper=1.)
    top.driver.add_constraint('cl_oper8', upper=1.)
    top.driver.add_constraint('cl_oper9', upper=1.)
    top.driver.add_constraint('cl_oper10', upper=1.)
    
    top.driver.add_constraint('MxTB_max', upper=1.)
    top.driver.add_constraint('MxTB_min', upper=1.)
    top.driver.add_constraint('MxTT_max', upper=1.)
    top.driver.add_constraint('MxTT_min', upper=1.)
    
    top.driver.add_constraint('Q_con', upper=1.)
    top.driver.add_constraint('T_con', upper=1.)
    top.driver.add_constraint('RPM_con', upper=1.)

    top.driver.add_objective('objf')

spl_st.configure()
spl_ae.configure()


# convenience functions to generate list of varnames to record
pf_vars = get_planform_recording_vars(with_CPs=True)
pf_stvars = get_planform_recording_vars(suffix='_st')
st_vars = get_structure_recording_vars(st3dn, with_props=True, with_CPs=True)

# add the recorder
from openmdao.api import SqliteRecorder, DumpRecorder
recorder = SqliteRecorder('optimization.sqlite')

recorder.options['includes'] = ['s', 's_st','blade_beam_structure',
                               'blade_mass', 'blade_mass_moment', 'aep',
                               'blade_scale', 'blade_length', 'objf','pb_con',
                               'tsr','cone_angle']
recorder.options['includes'] += pf_vars
recorder.options['includes'] += pf_stvars
recorder.options['includes'] += st_vars
recorder.options['includes'] += top.root.loads.aggregate.stat_var
recorder.options['includes'] += top.root.loads.aggregate.envelope_var
recorder.options['includes'] += top.root.envelope_correction.envelope_corr_var
recorder.options['includes'] += ['blade_failure_index', 'blade_failure_index_ks',
                                 'tip_tower_clearance']
if optimize_flag:
    recorder.options['includes'] += ['mass_con', 'mass_mom_con', 'maxchord_con',
                                     'maxsweep_con','tsr_scaler','blade_scaler']
    recorder.options['includes'] += ['T_con', 'Q_con', 'RPM_con']
    recorder.options['includes'] += ['MxTB_max','MxTB_min','MxTT_max','MxTT_min']
    recorder.options['includes'] += ['r04_width_con','r08_width_con','web_angle_diff',
                                     'TWconU','TWconL','pacc']
    recorder.options['includes'] += ['cl_oper1','cl_oper2','cl_oper3',
                                     'cl_oper4','cl_oper5','cl_oper6',
                                     'cl_oper7','cl_oper8','cl_oper9','cl_oper10']                  

top.driver.add_recorder(recorder)

t00 = time.time()
top.setup()
print('Setup time', time.time()-t00)

t0 = time.time()
top.run()
print('Total Execution time', time.time()-t0)

# --- 8 -----
if grad and optimize_flag:
    Jdict = {} 
    for i, dx in enumerate([2.5e-3, 5.e-3, 7.5e-3, 1.e-2, 1.5e-2, 2.e-2, 2.5e-2, 5.e-2, 7.5e-2, 1.e-1]):
        top.root.fd_options['step_size'] = dx 
        if MPI.COMM_WORLD.rank == 0:
            print('Calculating J for dx = %f'%dx)
            
        J = top.calc_gradient(top.driver.get_desvars().keys(),
                                 top.driver.get_objectives().keys()+top.driver.get_constraints().keys(),return_format='dict')
        Jdict[dx] = J.copy()          

    if MPI.COMM_WORLD.rank == 0:
        fid = open('Jdict.pkl', 'w') 
        import pickle
        pickle.dump(Jdict, fid) 
        fid.close()