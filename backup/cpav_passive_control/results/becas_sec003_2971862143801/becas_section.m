BECAS_SetupPath;
options.foldername=fullfile('/scratch/cpav/653765.jess.dtu.dk/becas_sec003_2971862143801/becas_inputs/BECAS_SECTION0.167');
load('becas_utils0.167.mat', 'utils', 'solutions', 'csprops')
theta0=[       598980.19584       1507787.35251       1139859.45833      -50207014.7458                   0      -826519.422393]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure0.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      569155.931413       1120173.40841       468881.971786      -43759695.6377      -25264672.0561      -1718444.27285]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure1.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      590560.604589        435099.44079       455442.154569      -13409478.9089      -23225898.7732      -23697.1579338]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure2.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      585715.962381       177431.220573        506918.91886                   0      -20088246.9805       654598.659058]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure3.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      573854.380104      -869187.990906       574384.044509       15062287.8124      -26088647.7694       863227.172113]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure4.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      334490.160569      -723432.134842        811101.26317       28487048.8892      -16447005.3446       1237954.43876]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure5.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[     -14469.4144231      -709465.428901       751622.527746       33985395.9891                   0      -530583.118058]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure6.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      333973.586316      -715741.337097       478105.339736       27707901.4846       15997164.3808       1527025.19156]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure7.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      565614.896103      -130615.648402       584863.491908        14037846.376       24314263.1522       1109309.44095]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure8.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      585715.962381       177431.220573        506918.91886                   0       21707614.9151       654598.659058]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure9.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      587862.000988       608288.075861       430587.146515      -21022798.0986        36412554.424      -82175.7744093]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure10.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      536136.233613       1263706.73266       601173.394626      -45482374.2734       26259261.0301      -1704818.68559]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure11.out';
eval(['save ' FileName ' failure -ascii -double']);
exit;
