BECAS_SetupPath;
options.foldername=fullfile('/scratch/cpav/653765.jess.dtu.dk/becas_sec014_2968849215537/becas_inputs/BECAS_SECTION0.778');
load('becas_utils0.778.mat', 'utils', 'solutions', 'csprops')
theta0=[      32992.1556469        100540.52808       4878.70945498      -466447.130157                   0       18733.5601756]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure0.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      41641.2987497       84253.9252578      -2180.89354665      -329680.677183      -190341.227718       17763.4108965]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure1.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      43174.8922449         133318.2753       -446.15950444      -190851.068068      -330563.746573       33669.0252405]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure2.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      45700.1476945       31784.4289796       -177970.65805                   0      -252127.136004       30203.2821675]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure3.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      48112.2850083       8226.32206482      -110617.417758       308521.094578      -534374.211016       32038.7224917]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure4.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      46952.6689332      -255633.886439      -191065.884295       1997549.41793      -1153285.69416      -37456.6512031]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure5.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[     -2179.62068943      -193047.940164      -32985.3274197       1714215.04916                   0      -120709.218925]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure6.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      27126.1953394      -174200.355968      -85726.1955091       1447145.35643       835509.761092       -63279.125911]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure7.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[       51149.838823      -28782.8778713      -69204.4717087       352889.999572       611223.408742      -13141.0088018]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure8.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      45700.1476945       31784.4289796       -177970.65805                   0       412141.552199       30203.2821675]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure9.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      43162.6748848       66666.8313935      -158.953511357      -192796.019407        333932.50111       24294.5698228]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure10.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      36559.5407861       95209.0275795       419.341758962      -417108.684334       240817.811181         17850.33724]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure11.out';
eval(['save ' FileName ' failure -ascii -double']);
exit;
