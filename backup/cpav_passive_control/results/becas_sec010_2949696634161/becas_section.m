BECAS_SetupPath;
options.foldername=fullfile('/scratch/cpav/653765.jess.dtu.dk/becas_sec010_2949696634161/becas_inputs/BECAS_SECTION0.556');
load('becas_utils0.556.mat', 'utils', 'solutions', 'csprops')
theta0=[      159093.057246       359531.095573       28529.0067008      -4707333.13435                   0      -446809.853237]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure0.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      164932.046173       273188.929043       56171.9698539      -3350184.77995      -1934230.08454      -236690.218642]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure1.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      164853.748217        128466.96439       63147.0775877      -1044391.02981       -1808938.3266      -23857.3093774]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure2.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[       165295.00023      -91536.4328758       67560.8418796                   0      -1505924.40676       76305.0849175]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure3.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      170296.657278      -54298.0855835       74008.4580935        2209862.1976      -3827593.60397       216171.594497]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure4.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      137463.696781      -400798.398671       134440.755814       7521679.33124      -4342643.58665       1284427.93427]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure5.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[        7095.239221      -295643.703878       96448.5244722        5982459.7035                   0       4491033.13927]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure6.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      113471.117244      -260429.981727       105803.546069       4938342.36148       2851153.29175        2102747.4021]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure7.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      175487.017967      -137099.600609        75722.452701       2519733.71493        4364306.8158       539033.621424]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure8.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[       165295.00023      -91536.4328758       67560.8418796                   0       2592972.41268       76305.0849175]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure9.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      159235.485632       202805.541807       61309.8286594      -2140401.13944       3707283.52209      -113108.229749]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure10.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      154435.851266       328022.528741       50767.3272919      -4284932.94124       2473907.18708      -346769.571057]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure11.out';
eval(['save ' FileName ' failure -ascii -double']);
exit;
