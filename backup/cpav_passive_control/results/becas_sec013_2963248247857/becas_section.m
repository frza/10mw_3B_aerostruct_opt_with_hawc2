BECAS_SetupPath;
options.foldername=fullfile('/scratch/cpav/653765.jess.dtu.dk/becas_sec013_2963248247857/becas_inputs/BECAS_SECTION0.722');
load('becas_utils0.722.mat', 'utils', 'solutions', 'csprops')
theta0=[      49895.7005788       147200.451946       3598.82901023      -947933.404268                   0      -29141.7672391]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure0.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      65293.3638183       127049.195614      -1131.27783122      -661901.628371      -382149.083317      -14777.3529585]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure1.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      67475.4136093       111364.024796      -22175.8074436      -309780.770905      -536556.034416       23031.3763816]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure2.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      69415.1010026       34750.4736108      -32870.8749805                   0      -386663.214312       41318.6088793]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure3.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      71993.6862484       5531.00098182      -43097.0358214       480004.604059      -831392.362098       57942.2643602]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure4.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      54134.7769267      -308431.850412      -91501.4052828       3478318.52525      -2008208.13688      -201215.465622]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure5.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[     -1403.22959896      -199243.682219      -58306.6017091       2635037.87536                   0      -68799.1550481]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure6.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      31368.3414969      -188097.518156      -53279.3605328       2227314.93976       1285940.88004       3599.53188806]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure7.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      75129.3909978       -45261.540966      -48303.3896717       605345.074776       1048488.42562       256477.766136]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure8.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      69415.1010026       34750.4736108      -32870.8749805                   0       671858.104409       41318.6088793]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure9.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      66162.6576604       92936.2910382      -21874.0005574      -402619.760283       697357.880941       10783.3516744]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure10.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      56449.3913102       143368.228766      -212.671692645      -837725.655589       483661.132761      -24169.9300925]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure11.out';
eval(['save ' FileName ' failure -ascii -double']);
exit;
