BECAS_SetupPath;
options.foldername=fullfile('/scratch/cpav/653765.jess.dtu.dk/becas_sec004_2932865344057/becas_inputs/BECAS_SECTION0.222');
load('becas_utils0.222.mat', 'utils', 'solutions', 'csprops')
theta0=[      536001.822216       1431166.58279       1023041.92956      -40078082.3183                   0      -481417.271741]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure0.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      523487.722908       1051888.77279       428973.934877      -34758533.1452       -20067848.468      -1321667.03773]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure1.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      532881.201349       402459.688119       411324.581493      -10729836.1376      -18584621.3471        39400.623016]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure2.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      516290.141405       136522.294026       455513.885978                   0      -16519720.9658       544124.550998]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure3.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      502959.512002      -71692.0181348       507650.987347       12532944.5684      -21707696.7609       649244.586845]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure4.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      280919.235669      -680279.417967       661553.259904       26006598.8021      -15014916.8191       730641.649867]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure5.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      -7043.5305535      -679368.888421       751691.952408       28953051.2161                   0       55682273.8245]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure6.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      310557.140602      -651351.266101       471876.410401       23736438.6763       13704239.2594       1262210.87952]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure7.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[       490050.94272      -175691.145958       515555.896755       11334219.0394       19631443.2403       882700.363567]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure8.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      516290.141405       136522.294026       455513.885978                   0       17438128.2593       544124.550998]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure9.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      524409.299034       589637.929451       389652.182131      -16618478.2268       28784048.6333      -13889.6963497]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure10.out';
eval(['save ' FileName ' failure -ascii -double']);
theta0=[      521751.146005       1052280.01066       431684.623407      -35876062.7165       20713054.4668      -1428539.74906]
%Calculate strains
[strain.GlobalElement,strain.MaterialElement] = BECAS_CalcStrainsElementCenter(theta0,solutions,utils);
%Calculate stresses
[ stress.GlobalElement, stress.MaterialElement ] = BECAS_CalcStressesElementCenter( strain, utils );
%Check failure criteria
[ failure ] = BECAS_CheckFailure( utils, stress.MaterialElement, strain.MaterialElement );
FileName='failure11.out';
eval(['save ' FileName ' failure -ascii -double']);
exit;
