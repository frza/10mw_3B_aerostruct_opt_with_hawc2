
# --- 1 -----

import numpy as np
import time
import os

from openmdao.core.mpi_wrap import MPI
from openmdao.api import Problem, Group, IndepVarComp, ParallelFDGroup, Component
from openmdao.api import ScipyOptimizer, ExecComp
from openmdao.solvers.ln_gauss_seidel import LinearGaussSeidel

if MPI:
    from openmdao.core.petsc_impl import PetscImpl as impl
else:
    from openmdao.core.basic_impl import BasicImpl as impl

from openmdao.drivers.pyoptsparse_driver import pyOptSparseDriver


from fusedwind.turbine.geometry import read_blade_planform,\
                                       redistribute_planform,\
                                       PGLLoftedBladeSurface,\
                                       SplinedBladePlanform, \
                                       PGLRedistributedPlanform
from fusedwind.turbine.structure import read_bladestructure, \
                                        write_bladestructure, \
                                        interpolate_bladestructure, \
                                        SplinedBladeStructure, \
                                        BladeStructureProperties
from fusedwind.turbine.recorders import get_planform_recording_vars, \
                                        get_structure_recording_vars

# from hawtopt2.main import HawtOpt2Main
from becas_wrapper.becas_bladestructure import BECASBeamStructure
from becas_wrapper.becas_stressrecovery import BECASStressRecovery
from hawc2_wrapper.hawc2s_aeroelasticsolver import HAWC2SAeroElasticSolver
from hawc2_wrapper.hawc2_aeroelasticsolver import HAWC2AeroElasticSolver

from sqlitedict import SqliteDict

user_home = os.getenv('HOME')


class WeibullAEP(Component):

    def __init__(self, size, wsp_array, A=8., k=2.):
        super(WeibullAEP, self).__init__()

        self.wsp_array = wsp_array
        self.A = A
        self.k = k
        self.exclude = []
        self.wspIn = wsp_array[0]
        self.wspOut = wsp_array[-1]

        self.add_param('power_curve', np.zeros(size))
        self.add_output('aep', 0.)

    def solve_nonlinear(self, params, unknowns, resids):

        cdf_ws = lambda A,k,u, bin=1.0: np.exp(-(max(0.0, u-bin/2.0)/A)**k) - np.exp(-((u+bin/2.0)/A)**k)

        ws_f = []
        ps = []
        wsps = []
        for i, ws in enumerate(self.wsp_array):
            if i in self.exclude: continue
            if ws > self.wspOut: continue
            if ws < self.wspIn: continue
            ws_f.append(cdf_ws(self.A, self.k, ws))
            ps.append(params['power_curve'][i])
            wsps.append(ws)
        self.ws_f = np.asarray(ws_f)
        ps = np.asarray(ps)
        wsps = np.asarray(wsps)

        unknowns['aep'] = np.trapz(ps * self.ws_f * 8760.0, wsps)


class H2PowerCurve(Component):

    def __init__(self, nwsp, nchannels, ncases, ch_nr):
        super(H2PowerCurve, self).__init__()

        self.nwsp = nwsp
        self.ch_nr = ch_nr

        self.add_param('mean_stats', shape=[ncases,nchannels])
        self.add_output('P', np.zeros(nwsp))

    def solve_nonlinear(self, params, unknowns, resids):

        unknowns['P'] = params['mean_stats'][:self.nwsp,self.ch_nr]


class CorrectedEnvelopes(Component):

    def __init__(self, nsec, nsectors, full_dict, red_dict):
        super(CorrectedEnvelopes, self).__init__()

        self.full_dict = full_dict
        self.red_dict = red_dict
        self.nsec = nsec

        self.envelope_corr_var = []

        db_full = SqliteDict(self.full_dict, 'openmdao' )
        self.us_full = db_full['rank0:Driver/1']['Unknowns']

        db_red = SqliteDict(self.red_dict, 'openmdao' )
        self.us_red = db_red['rank0:Driver/1']['Unknowns']

        for isec in range(nsec):
            p_name = 'blade_loads_envelope_sec%03d_ori' %isec
            self.add_param(p_name, shape=[nsectors, 6])
            name = 'blade_loads_envelope_sec%03d_corr' %isec
            self.envelope_corr_var.append(name)
            self.add_output(name, shape=[nsectors, 6])

    def solve_nonlinear(self, params, unknowns, resids):

        for i, var in enumerate(self.envelope_corr_var):
            theta = params['blade_loads_envelope_sec%03d_ori'%i]\
                                    /self.us_red['blade_loads_envelope_sec%03d'%i]\
                                    *self.us_full['blade_loads_envelope_sec%03d'%i]
            unknowns[var] = np.nan_to_num(theta)



# class TipDeflection(Component):
# 
#     def __init__(self, nwsp, size):
#         super(TipDeflection, self).__init__()
# 
#         self.add_param('cone_angle', 0.)


optimize_flag = True
dry_run = False
FPM = False
par_fd = 20

if optimize_flag:
    top = Problem(impl=impl, root=ParallelFDGroup(par_fd))
else:
    top = Problem(impl=impl, root=Group())

root = top.root
root.ln_solver = LinearGaussSeidel()
#root.ln_solver.options['single_voi_relevance_reduction'] = True

opt = True
if opt and optimize_flag:
    top.driver = pyOptSparseDriver()
    top.driver.options['optimizer'] = 'IPOPT'
    top.driver.opt_settings['mu_strategy'] = 'adaptive'
    top.driver.opt_settings['max_iter'] = 150

    # top.driver.opt_settings['nlp_scaling_method'] = 'none'
    # top.driver.opt_settings['print_level'] = 2

    # top.driver.opt_settings['file_print_level'] = 5
    # top.driver.opt_settings['nlp_scaling_method'] = 'none'
    # #
    # top.driver.opt_settings['derivative_test'] = 'first-order'
    # top.driver.opt_settings['point_perturbation_radius'] = .2
    # top.driver.opt_settings['max_iter'] = 10
    # top.driver.opt_settings['derivative_test_print_all'] = 'yes'
    # top.driver.opt_settings['findiff_perturbation'] = 1.e-2

#   top.driver.options['optimizer'] = 'SNOPT'
#   top.driver.opt_settings = {'Major step limit': 0.2,
#                           #  'Major Print level':000011,
#                           #  'Minor Print level':11,
#                           # 'Difference interval':1.e-3,
#                           #  'Print frequency': 1,
#                           #  'Summary frequency':1,
#                           #  'System information': 'Yes',
#                            'Iterations limit': 100000,
#                             'Major iterations limit': 100,
#                             'Verify level': -1}
    top.driver.hist_file = 'pyopt_hist.dat'

top.root.fd_options['force_fd'] = True
top.root.fd_options['step_size'] = 1.e-2
# --- 3 -----

# basic geometry
root.add('hub_height_c', IndepVarComp('hub_height', 119., units='m'), promotes=['*'])
root.add('cone_c', IndepVarComp('cone_angle', 2.5, units='m'), promotes=['*'])
root.add('hub_radius_c', IndepVarComp('hub_radius', 2.8, units='m'), promotes=['*'])
root.add('hub_scale_c', IndepVarComp('blade_scaler', 0.), promotes=['*'])
# dependent variables
root.add('blade_scaler_c', ExecComp('blade_scale = 1. + blade_scaler * 0.01'), promotes=['*'])
root.add('blade_length_c', ExecComp('blade_length = 86.366 * blade_scale'), promotes=['*'])

root.add('tsr_scaler_c', IndepVarComp('tsr_scaler', 1.), promotes=['*'])
root.add('tsr_c', ExecComp('tsr = tsr_scaler * 7.5'), promotes=['*'])

# --- 3 -----

# Geometry

pf = read_blade_planform('data/DTU_10MW_RWT_blade_axis_prebend.dat')
nsec_ae = 30
nsec_st = 20
dist = np.array([[0., 1./nsec_ae, 1], [1., 1./nsec_ae/3., nsec_ae]])
from PGL.main.distfunc import distfunc
s_ae = distfunc(dist)
s_st = np.linspace(0, 1, nsec_st)
pf = redistribute_planform(pf, s=s_ae)

# add planform spline component
spl_ae = root.add('pf_splines', SplinedBladePlanform(pf), promotes=['*'])

# component for interpolating planform onto structural mesh
redist = root.add('pf_st', PGLRedistributedPlanform('_st', nsec_ae, s_st), promotes=['*'])

# configure blade surface for structural solver
cfg = {}
cfg['redistribute_flag'] = False
cfg['blend_var'] = np.array([0.241, 0.301, 0.36, 0.48, 0.72, 1.0])
afs = []
for f in ['data/ffaw3241.dat',
          'data/ffaw3301.dat',
          'data/ffaw3360.dat',
          'data/ffaw3480.dat',
          'data/tc72.dat',
          'data/cylinder.dat']:

    afs.append(np.loadtxt(f))
cfg['base_airfoils'] = afs
surf = root.add('blade_surf', PGLLoftedBladeSurface(cfg, size_in=nsec_st,
                                size_out=(200, nsec_st, 3), suffix='_st'), promotes=['*'])

# read the blade structure
st3d = read_bladestructure('data/DTU10MW')

# and interpolate onto new distribution
st3dn = interpolate_bladestructure(st3d, s_st)

spl_st = root.add('st_splines', SplinedBladeStructure(st3dn), promotes=['*'])

st_props = root.add('st_props', BladeStructureProperties((200, nsec_st, 3), st3dn, [4,5,8,9]), promotes=['*'])
# --- 4 -----

# inputs to CS2DtoBECAS and BECASWrapper
config = {}
cfg = {}
cfg['dry_run'] = dry_run
cfg['dominant_elsets'] = ['REGION04', 'REGION08']
cfg['max_layers'] = 8
config['CS2DtoBECAS'] = cfg
cfg = {}
cfg['hawc2_FPM'] = FPM
cfg['plot_paraview'] = False
cfg['dry_run'] = dry_run
cfg['analysis_mode'] = 'stiffness'
cfg['checkmesh'] = False
config['BECASWrapper'] = cfg

root.add('stiffness', BECASBeamStructure(root, config, st3dn, (200, nsec_st, 3)), promotes=['*'])

# --- 5 -----

# inputs to HAWC2S
config = {}
config['master_file'] = 'htc/master_hs2.htc'
config['with_structure'] = True
config['with_geom'] = True
config['with_tsr'] = True
# config['with_cone'] = True
config['aerodynamic_sections'] = nsec_ae
config['structural_sections'] = nsec_st
cf = {}
config['HAWC2SInputWriter'] = cf
cf = {}
cf['dry_run'] = False
cf['copyback_results'] = False
cf['hawc2bin'] = 'HAWC2S.exe'
cf['verbose'] = True
config['HAWC2Wrapper'] = cf

cf = {}
wsp_array = [5, 6, 7, 8, 9,9.5, 10, 10.5, 11, 12, 13, 15, 17, 20, 22]
# wsp_array = [5, 7, 9]
nwsp = len(wsp_array)+1
cf['wsp'] = wsp_array
cf['user'] = [{'wsp':15., 'pitch':0., 'rpm':9.6}]
config['cases'] = cf

cf = {}
cf['blade'] = ['aoa', 'Ft', 'Fn', 'cl', 'cd',  'cp', 'ct', 'disp_x', 'disp_y', 'disp_z', 'disp_rot_z']
cf['rotor'] = ['wsp', 'pitch', 'rpm', 'P', 'Q', 'T', 'CP',  'CT', 'Mx']
config['HAWC2SOutputs'] = cf

root.add('hs2', HAWC2SAeroElasticSolver(config), promotes=['*'])

# --- 7 -----

# AEP

root.add('aep_calc', WeibullAEP(nwsp, wsp_array, A=8., k=2.), promotes=['aep'])
root.aep_calc.exclude = [15]
root.connect('P', 'aep_calc.power_curve')

# --- 8 -----
# General options
config = {}
config['master_file'] = 'htc/master_h2_simple.htc'

config['with_tsr'] = True
# config['with_cone'] = True
config['with_structure'] = True
config['with_geom'] = True
config['aerodynamic_sections'] = nsec_ae
config['structural_sections'] = nsec_st

# HAWC2GeometryBuilder
cf = {}
cf['interp_from_htc'] = False
config['HAWC2GeometryBuilder'] = cf

# HAWC2Wrapper
cf = {}
cf['dry_run'] = False
cf['copyback_results'] = False
cf['hawc2bin'] = 'HAWC2mb.exe'
cf['verbose'] = True
cf['timeout'] = 200.
config['HAWC2Wrapper'] = cf

# HAWC2Outputs
cf = {}
cf['channels'] = ['tower-tower-node-001-momentvec-x',
                  'tower-tower-node-001-momentvec-y',
                  'tower-tower-node-011-momentvec-x',
                  'tower-tower-node-011-momentvec-y',
                  'blade1-blade1-node-003-momentvec-x',
                  'blade1-blade1-node-003-momentvec-y',
                  'bearing-shaft_rot-angle_speed-rpm',
                  'bearing-pitch1-angle-deg',
                  'global-blade1-elem-019-zrel-1.00-State pos-y',
                  'global-blade2-elem-019-zrel-1.00-State pos-y',
                  'global-blade3-elem-019-zrel-1.00-State pos-y',
                  'blade1-blade1-elem-019-zrel-1.00-State pos-y',
                  'blade2-blade2-elem-019-zrel-1.00-State pos-y',
                  'blade3-blade3-elem-019-zrel-1.00-State pos-y',
                  'DLL-5-inpvec-1']
nchannels = len(cf['channels'])
cf['ch_envelope'] = []
cf['nr_blades_out'] = 3
cf['Nsectors'] = 12

for nsec in range(1, config['structural_sections']+1):
    for nrb in range(1, cf['nr_blades_out']+1):
        cf['ch_envelope'].append(\
    ['blade%i-blade%i-node-%03d-momentvec-x' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-momentvec-y' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-momentvec-z' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-forcevec-x' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-forcevec-y' % (nrb, nrb, nsec),
     'blade%i-blade%i-node-%03d-forcevec-z' % (nrb, nrb, nsec)])
cf['psf'] = {}
cf['dry_run'] = False

config['HAWC2Outputs'] = cf

# HAWC2InputWriter
cf = {}
cf['turb_directory'] = './turb'
config['HAWC2InputWriter'] = cf


root.add('loads', HAWC2AeroElasticSolver(config, 'DLCs', dlcs_fext='xls'), promotes=['*'])
ncases = root.loads.ncases


config = {}
cfg = {}
cfg['hawc2_FPM'] = FPM
cfg['plot_paraview'] = False
cfg['dry_run'] = False
cfg['analysis_mode'] = 'stress_recovery'
config['BECASWrapper'] = cfg

root.add('stress_recovery', BECASStressRecovery(config, s_st, 12), promotes=['*'])
# root.add('envelope_correction', CorrectedEnvelopes(nsec_st, 12,
#                     'sqlite_envelopes/full_envelopes.sqlite',
#                     'sqlite_envelopes/simple_envelopes.sqlite'),promotes=['*'])

for i in range(nsec_st):
    root.connect('blade_loads_envelope_sec%03d'%i, 'load_cases_sec%03d'%i)
    # root.connect('blade_loads_envelope_sec%03d'%i,'blade_loads_envelope_sec%03d_ori'%i)
    # root.connect('blade_loads_envelope_sec%03d_corr'%i, 'load_cases_sec%03d'%i)

# --- 6 -----

# --- 7 -----

factor = 0.925
mass_ref = 41740.846
# mass_ref = 42576.43041308516
aep_ref = 31710358139.748501
aep = ExecComp('objf = -(aep / %f)' % (aep_ref))

root.add('objf_c', aep, promotes=['*'])

# read reference values
from sqlitedict import SqliteDict
db = SqliteDict('dlb_simple_reference.sqlite', 'openmdao' )
us = db['rank0:Driver/1']['Unknowns']
tip_max = np.max([us['stat_max'][:,11].max(),
                      us['stat_max'][:,12].max(),
                      us['stat_max'][:,13].max()])*1.01

root.add('tip_pos_yc', ExecComp('tip_pos_y=numpy.max(disp_y[:])/5.', disp_y=np.zeros((nwsp, 28))), promotes=['*'])
root.add('Mx_norm', ExecComp('Mx_con=Mx[:]/%.20e' % -0.4230748538221593E+08, Mx_con=np.zeros(nwsp), Mx=np.zeros(nwsp)), promotes=['*'])
root.add('T_norm', ExecComp('T_con=T[:]/%.20e' % 0.2200385542377141E+07, T_con=np.zeros(nwsp), T=np.zeros(nwsp)), promotes=['*'])
root.add('Q_norm', ExecComp('Q_con=Q[:-1]/%.20e' % (1.001*10638.3e3/(9.6*np.pi/30.)), Q_con=np.zeros(nwsp-1), Q=np.zeros(nwsp)), promotes=['*'])
root.add('loads_tower_clearance_c', ExecComp('tip_tower_clearance=stat_min[:,-1]/10.', tip_tower_clearance=np.zeros(ncases),
                                                                                      stat_min=np.zeros((ncases, nchannels))), promotes=['*'])
root.add('tip_pos_b1_c', ExecComp('tip_pos_b1=stat_max[:12,11]/%f'%tip_max, tip_pos_b1=np.zeros(12),
                                                                                                 stat_max=np.zeros((ncases, nchannels))), promotes=['*'])
root.add('tip_pos_b2_c', ExecComp('tip_pos_b2=stat_max[:12,12]/%f'%tip_max, tip_pos_b2=np.zeros(12),
                                                                                                 stat_max=np.zeros((ncases, nchannels))), promotes=['*'])
root.add('tip_pos_b3_c', ExecComp('tip_pos_b3=stat_max[:12,13]/%f'%tip_max, tip_pos_b3=np.zeros(12),
                                                                                                 stat_max=np.zeros((ncases, nchannels))), promotes=['*'])
root.add('MxBR_max_c', ExecComp('MxBR_max=(stat_max[:,4])/%f'%us['stat_max'][:,4].max(), MxBR_max=np.zeros(ncases), stat_max=np.zeros((ncases, nchannels))), promotes=['*'])
root.add('MxBR_min_c', ExecComp('MxBR_min=(stat_min[:,4])/%f'%us['stat_min'][:,4].min(), MxBR_min=np.zeros(ncases), stat_min=np.zeros((ncases, nchannels))), promotes=['*'])
root.add('MyBR_max_c', ExecComp('MyBR_max=(stat_max[:,5])/%f'%us['stat_max'][:,5].max(), MyBR_max=np.zeros(ncases), stat_max=np.zeros((ncases, nchannels))), promotes=['*'])
root.add('MyBR_min_c', ExecComp('MyBR_min=(stat_min[:,5])/%f'%us['stat_min'][:,5].min(), MyBR_min=np.zeros(ncases), stat_min=np.zeros((ncases, nchannels))), promotes=['*'])
root.add('MxTB_max_c', ExecComp('MxTB_max=(stat_max[:,0])/%f'%us['stat_max'][:,0].max(), MxTB_max=np.zeros(ncases), stat_max=np.zeros((ncases, nchannels))), promotes=['*'])
root.add('MxTB_min_c', ExecComp('MxTB_min=(stat_min[:,0])/%f'%us['stat_min'][:,0].min(), MxTB_min=np.zeros(ncases), stat_min=np.zeros((ncases, nchannels))), promotes=['*'])
root.add('MyTB_max_c', ExecComp('MyTB_max=(stat_max[:,1])/%f'%us['stat_max'][:,1].max(), MyTB_max=np.zeros(ncases), stat_max=np.zeros((ncases, nchannels))), promotes=['*'])
root.add('MyTB_min_c', ExecComp('MyTB_min=(stat_min[:,1])/%f'%us['stat_min'][:,1].min(), MyTB_min=np.zeros(ncases), stat_min=np.zeros((ncases, nchannels))), promotes=['*'])
root.add('massmom_con', ExecComp('mass_mom_con = blade_mass_moment/%f'%us['blade_mass_moment']), promotes=['*'])
root.add('mass_conc', ExecComp('mass_con = blade_mass/%f'%us['blade_mass']), promotes=['*'])

# [5, 6, 7, 8, 9,9.5, 10, 10.5, 11, 12, 13, 15, nwsp, 20, 22, 25]
root.add('cl_oper8_c', ExecComp('cl_oper8=numpy.interp([0.4, 0.6, 0.7, 0.8, 0.85, 0.9, 0.95], s[1:-1], cl[3,:])', s=np.zeros(nsec_ae),
                                                                                                           cl=np.zeros((nwsp,nsec_ae-2)),
                                                                                                           cl_oper8=np.zeros(7)),
                                                                                                           promotes=['*'])
root.add('cl_oper9_c', ExecComp('cl_oper9=numpy.interp([0.4, 0.6, 0.7, 0.8, 0.85, 0.9, 0.95], s[1:-1], cl[4,:])', s=np.zeros(nsec_ae),
                                                                                                           cl=np.zeros((nwsp,nsec_ae-2)),
                                                                                                           cl_oper9=np.zeros(7)),
                                                                                                           promotes=['*'])
root.add('cl_oper10_c', ExecComp('cl_oper10=numpy.interp([0.4, 0.6, 0.7, 0.8, 0.85, 0.9, 0.95], s[1:-1], cl[6,:])', s=np.zeros(nsec_ae),
                                                                                                           cl=np.zeros((nwsp,nsec_ae-2)),
                                                                                                           cl_oper10=np.zeros(7)),
                                                                                                           promotes=['*'])
root.add('cl_oper11_c', ExecComp('cl_oper11=numpy.interp([0.4, 0.6, 0.7, 0.8, 0.85, 0.9, 0.95], s[1:-1], cl[8,:])', s=np.zeros(nsec_ae),
                                                                                                           cl=np.zeros((nwsp,nsec_ae-2)),
                                                                                                           cl_oper11=np.zeros(7)),
                                                                                                           promotes=['*'])

root.add('W08_c', IndepVarComp('width_08_ref', us['r08_width']*us['blade_length']), promotes=['*'])
root.add('W04_c', IndepVarComp('width_04_ref', us['r04_width']*us['blade_length']), promotes=['*'])
root.add('TWu_c', IndepVarComp('TWu', np.minimum(us['r08_thickness']/(us['r08_width']*us['blade_length']), 0.08)), promotes=['*'])
root.add('TWl_c', IndepVarComp('TWl', np.minimum(us['r04_thickness']/(us['r04_width']*us['blade_length']), 0.08)), promotes=['*'])
root.add('TWconL_c', ExecComp('TWconL=r04_thickness/(r04_width*blade_length)-TWl', r04_thickness=np.zeros(20),
                                                                               r04_width=np.zeros(20),
                                                                               TWl=np.zeros(20),
                                                                               TWconL=np.zeros(20)), promotes=['*'])
root.add('TWconU_c', ExecComp('TWconU=r08_thickness/(r08_width*blade_length)-TWu', r08_thickness=np.zeros(20),
                                                                               r08_width=np.zeros(20),
                                                                               TWu=np.zeros(20),
                                                                               TWconU=np.zeros(20)), promotes=['*'])
root.add('web_diff02_c', ExecComp('web_angle_diff=web_angle02-web_angle03', web_angle02=np.zeros(20),
                                                                        web_angle03=np.zeros(20),
                                                                        web_angle_diff=np.zeros(20)), promotes=['*'])
root.add('pacc_c', ExecComp('pacc=0.5*(pacc_u[:,0]+pacc_l[:,0])', pacc_u=np.zeros((20,2)), pacc_l=np.zeros((20,2)),pacc=np.zeros(20)), promotes=['*'])
root.add('r08_width_con_c', ExecComp('r08_width_con=r08_width*blade_length/width_08_ref', r08_width=np.zeros(20), width_08_ref=np.zeros(20),r08_width_con=np.zeros(20)), promotes=['*'])
root.add('r04_width_con_c', ExecComp('r04_width_con=r04_width*blade_length/width_04_ref', r04_width=np.zeros(20), width_04_ref=np.zeros(20),r04_width_con=np.zeros(20)), promotes=['*'])

if optimize_flag:

    # add blade structure splines
    spl_st.add_spline('DP04', np.array([0., 0.2, 0.5, 0.85, 1.]), spline_type='bezier', scaler=2.)
    spl_st.add_spline('DP05', np.array([0., 0.2, 0.5, 0.85, 1.]), spline_type='bezier', scaler=2.)
    spl_st.add_spline('DP08', np.array([0., 0.2, 0.5, 0.85, 1.]), spline_type='bezier', scaler=2.)
    spl_st.add_spline('DP09', np.array([0., 0.2, 0.5, 0.85, 1.]), spline_type='bezier', scaler=2.)
    spl_st.add_spline(('r04uniax00T', 'r04uniax01T',
                       'r08uniax00T', 'r08uniax01T'),
                       np.array([0., 0.2, 0.55, 0.85, 1.]), spline_type='bezier', scaler=1.)
    spl_st.add_spline(('r00uniax00T', 'r00uniax01T',
                       'r01uniax00T', 'r01uniax01T',
                       'r11uniax00T', 'r11uniax01T',
                       'r12uniax00T', 'r12uniax01T',
                       'w00uniax00T', 'w00uniax01T'),
                       np.array([0., 0.25, 0.8, 1.]), spline_type='bezier', scaler=0.004)
    spl_st.add_spline(('r00triax00T', 'r00triax01T',
                       'r01triax00T', 'r01triax01T',
                       'r11triax00T', 'r11triax01T',
                       'r12triax00T', 'r12triax01T',
                       'w00triax00T', 'w00triax01T'),
                       np.array([0., 0.25, 0.8, 1.]), spline_type='bezier', scaler=0.004)
    spl_st.add_spline(('r02triax00T', 'r02triax01T',
                       'r03triax00T', 'r03triax01T',
                       'r09triax00T', 'r09triax01T',
                       'r10triax00T', 'r10triax01T'),
                       np.array([0., 0.25, 0.8, 1.]), spline_type='bezier', scaler=0.004)
    spl_st.add_spline(('r02uniax00T', 'r02uniax01T',
                       'r03uniax00T', 'r03uniax01T',
                       'r05uniax00T', 'r05uniax01T',
                       'r07uniax00T', 'r07uniax01T',
                       'r09uniax00T', 'r09uniax01T',
                       'r10uniax00T', 'r10uniax01T'),
                       np.array([0., 0.15, 0.3]), spline_type='bezier', scaler=0.004)
    spl_st.add_spline(('r05triax00T', 'r05triax01T',
                       'r06triax00T', 'r06triax01T',
                       'r07triax00T', 'r07triax01T'),
                       np.array([0., 0.25, 0.8, 1.]), spline_type='bezier', scaler=0.004)
    spl_st.add_spline(('r06uniax00T', 'r06uniax01T'),
                       np.array([0., 0.25, 0.8, 1.]), spline_type='bezier', scaler=0.004)

    # 36
    top.driver.add_desvar('DP04_C', lower=-.5, upper=0.5, indices=[0,1,2,3,4])
    top.driver.add_desvar('DP05_C', lower=-.5, upper=0.5, indices=[0,1,2,3,4])
    top.driver.add_desvar('DP08_C', lower=-.5, upper=0.5, indices=[0,1,2,3,4])
    top.driver.add_desvar('DP09_C', lower=-.5, upper=0.5, indices=[0,1,2,3,4])
    top.driver.add_desvar('r04uniax00T_C', lower=-2., upper=2., indices=[0,1,2,3])
    top.driver.add_desvar('r00uniax00T_C', lower=-2., upper=2., indices=[1,2])
    top.driver.add_desvar('r00triax00T_C', lower=-2., upper=2., indices=[1,2])
    top.driver.add_desvar('r02triax00T_C', lower=-2., upper=2., indices=[1,2])
    top.driver.add_desvar('r02uniax00T_C', lower=-2., upper=2., indices=[0,1])
    top.driver.add_desvar('r05triax00T_C', lower=-2., upper=2., indices=[1,2])
    top.driver.add_desvar('r06uniax00T_C', lower=-2., upper=2., indices=[1,2])

    # add blade planform splines
    spl_ae.add_spline('chord', np.array([0., 0.25, 0.45, 0.65, 0.9, 1]), spline_type='bezier', scaler=0.1)
    spl_ae.add_spline('rthick', np.array([0., 0.25, 0.6, 0.85, 1]), spline_type='bezier', scaler=1.)
    spl_ae.add_spline('rot_z', np.array([0., 0.25, 0.45, 0.65, 0.9, 1]), spline_type='bezier', scaler=10.)
    spl_ae.add_spline('y', np.array([0., 0.25, 0.45, 0.65, 0.9, 1]), spline_type='bezier', scaler=.1)
    spl_ae.add_spline('p_le', np.array([0., 0.25, 0.65,  1.]), spline_type='bezier', scaler=.5)

    # add design variables
    # 24 dvs
    top.driver.add_desvar('chord_C', lower=-1, upper=1, indices=[0,1,2,3,4,5])
    top.driver.add_desvar('rthick_C', lower=-1., upper=1., indices=[1,2,3])
    top.driver.add_desvar('rot_z_C', lower=-1, upper=1, indices=[1,2,3,4,5])
    top.driver.add_desvar('y_C', lower=-1.2, upper=1.2, indices=[2,3,4,5])
    top.driver.add_desvar('p_le_C', lower=-0.2, upper=0.2, indices=[1,2,3])
    top.driver.add_desvar('tsr_scaler', lower=0.8, upper=1.5)
#     top.driver.add_desvar('cone_angle', lower=0.0, upper=5.)
    top.driver.add_desvar('blade_scaler', lower=-10, upper=20.)

    root.add('chord_conc', ExecComp('maxchord_con = chord * blade_scale', chord=np.zeros(30), maxchord_con=np.zeros(30)), promotes=['*'])
    root.add('min_chord_conc', ExecComp('minchord_con = chord[-1] * blade_scale', chord=np.zeros(30)), promotes=['*'])
    root.add('rthick_conmc', ExecComp('rthick_con_max = numpy.interp([0.3, 0.4, 0.5, 0.6], s, rthick)',
                                      s=np.zeros(30),
                                      rthick=np.zeros(30),
                                      rthick_con_max=np.zeros(4)), promotes=['*'])
    root.add('rthick_conc', ExecComp('rthick_con = numpy.interp([0.25, 0.4, 0.6, 0.8, 0.85, 0.9, 0.95, 1.], s, rthick)',
                                      s=np.zeros(30),
                                      rthick=np.zeros(30),
                                      rthick_con=np.zeros(8)), promotes=['*'])
    root.add('pb_conc', ExecComp('pb_con = -numpy.min(y) * blade_scale / 0.072', y=np.zeros(30)), promotes=['*'])
    top.driver.add_constraint('maxchord_con', upper=0.072)
    top.driver.add_constraint('minchord_con', lower=0.002315726096, upper=0.01736794572)
    top.driver.add_constraint('y_curv', upper=0.)
    top.driver.add_constraint('rthick', lower=0.2400)
    # top.driver.add_constraint('tip_tower_clearance', lower=us['tip_tower_clearance'].min()-0.05)
    # top.driver.add_constraint('tip_pos_b1', upper=1.0)
    # top.driver.add_constraint('tip_pos_b2', upper=1.0)
    # top.driver.add_constraint('tip_pos_b3', upper=1.0)
    top.driver.add_constraint('tip_pos_y', upper=1.)
    top.driver.add_constraint('pb_con', upper=1.)
    top.driver.add_constraint('Mx_con', upper=1.005)
    top.driver.add_constraint('Q_con', upper=1.)
    top.driver.add_constraint('T_con', upper=1.005)
    top.driver.add_constraint('mass_mom_con', upper=1.01)
    top.driver.add_constraint('mass_con', upper=1.01)
    top.driver.add_constraint('cl_oper8', upper=1.35)
    top.driver.add_constraint('cl_oper9', upper=1.35)
    top.driver.add_constraint('cl_oper10', upper=1.35)
    top.driver.add_constraint('cl_oper11', upper=1.35)
    top.driver.add_constraint('rthick_con', upper=0.25, indices=[4, 5, 6])
    top.driver.add_constraint('rthick_con_max', upper=0.361)
    # top.driver.add_constraint('MxBR_max', upper=1.01)
    # top.driver.add_constraint('MxBR_min', upper=1.01)
    # top.driver.add_constraint('MyBR_max', upper=1.01)
    # top.driver.add_constraint('MyBR_min', upper=1.01)
    # top.driver.add_constraint('MxTB_max', upper=1.01)
    # top.driver.add_constraint('MxTB_min', upper=1.01)
    # top.driver.add_constraint('MyTB_max', upper=1.0)
    # top.driver.add_constraint('MyTB_min', upper=1.0)

    top.driver.add_constraint('TWconU', lower=0.)
    top.driver.add_constraint('TWconL', lower=0.)
    top.driver.add_constraint('r04_width_con', lower=0.8)
    top.driver.add_constraint('r08_width_con', lower=0.8)
    top.driver.add_constraint('web_angle_diff', lower=-0.5, upper=0.5)

    top.driver.add_constraint('r03_width', lower=5.e-4)
    top.driver.add_constraint('r05_width', lower=5.e-4)
    top.driver.add_constraint('r07_width', lower=5.e-4)
    top.driver.add_constraint('r04_width', lower=5.e-4)
    top.driver.add_constraint('r08_width', lower=5.e-4)
    top.driver.add_constraint('r09_width', lower=5.e-4)
    top.driver.add_constraint('r00_thickness', lower=1.e-3)
    top.driver.add_constraint('r01_thickness', lower=1.e-3)
    top.driver.add_constraint('r02_thickness', lower=1.e-3)
    top.driver.add_constraint('r03_thickness', lower=1.e-3)
    top.driver.add_constraint('r04_thickness', lower=1.e-3)
    top.driver.add_constraint('r05_thickness', lower=1.e-3)
    top.driver.add_constraint('r06_thickness', lower=1.e-3)
    top.driver.add_constraint('r00uniax00T', lower=-1.e-6)
    top.driver.add_constraint('r00triax00T', lower=-1.e-6)
    top.driver.add_constraint('r01uniax00T', lower=-1.e-6)
    top.driver.add_constraint('r01triax00T', lower=-1.e-6)
    top.driver.add_constraint('r02triax00T', lower=-1.e-6)
    top.driver.add_constraint('r02uniax00T', lower=-1.e-6)
    top.driver.add_constraint('r03triax00T', lower=-1.e-6)
    top.driver.add_constraint('r03uniax00T', lower=-1.e-6)
    top.driver.add_constraint('r04uniax00T', lower=-1.e-6, upper=0.06)
    top.driver.add_constraint('r05triax00T', lower=-1.e-6)
    top.driver.add_constraint('r06triax00T', lower=-1.e-6)
    top.driver.add_constraint('r06uniax00T', lower=-1.e-6)
    top.driver.add_constraint('web_angle02', upper=1.e-1)
    top.driver.add_constraint('web_angle03', upper=1.e-1)
    top.driver.add_constraint('pacc', lower=-0.0001, upper=0.0001, indices=[0])

    # blade failure
    top.driver.add_constraint('blade_failure_index_ks', upper=0.9)

    top.driver.add_objective('objf')

spl_st.configure()
spl_ae.configure()


# convenience functions to generate list of varnames to record
pf_vars = get_planform_recording_vars(with_CPs=True)
pf_stvars = get_planform_recording_vars(suffix='_st')
st_vars = get_structure_recording_vars(st3dn, with_props=True, with_CPs=True)

# add the recorder
from openmdao.api import SqliteRecorder, DumpRecorder
recorder = SqliteRecorder('optimization.sqlite')

recorder.options['includes'] = ['s', 's_st','blade_beam_structure', 'blade_mass', 'blade_mass_moment', 'aep','blade_scale', 'blade_length', 'objf',
                                'Q_con', 'Mx_con', 'T_con','T_con_ex', 'tip_pos_y', 'tip_pos_z', 'tip_pos_y_ex', 'pb_con']
recorder.options['includes'] += ['tsr','Mx_con_ex','TWconU','TWconL', 'mass_con', 'mass_mom_con']
recorder.options['includes'] += ['aoa', 'Ft', 'Fn', 'cl', 'cd',  'cp', 'ct', 'disp_x', 'disp_y', 'disp_z', 'disp_rot_z']
recorder.options['includes'] += ['wsp', 'pitch', 'rpm', 'P', 'T', 'CP',  'CT', 'Mx', 'blade_scaler', 'web_angle_diff']
recorder.options['includes'] += pf_vars
recorder.options['includes'] += pf_stvars
recorder.options['includes'] += st_vars
recorder.options['includes'] += ['blade_failure_index', 'blade_failure_index_ks', 'tip_tower_clearance', 'tip_pos_b1', 'tip_pos_b2', 'tip_pos_b3', 'rthick_con_max', 'rthick_con','pacc']
recorder.options['includes'] += top.root.loads.aggregate.stat_var
recorder.options['includes'] += top.root.loads.aggregate.fatigue_var
recorder.options['includes'] += top.root.loads.aggregate.envelope_var
# recorder.options['includes'] += top.root.envelope_correction.envelope_corr_var

recorder.options['includes'] += ['MxBR_max',
                                 'MxBR_min',
                                 'MyBR_max',
                                 'MyBR_min',
                                 'MxTB_max',
                                 'MxTB_min',
                                 'MyTB_max',
                                 'MyTB_min']
top.driver.add_recorder(recorder)

t00 = time.time()
top.setup()
print('Setup time', time.time()-t00)

t0 = time.time()
top.run()
print('Total Execution time', time.time()-t0)

# --- 8 -----
